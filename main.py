import re
import json
import smtplib
import requests
import threading
import configparser
import dns.resolver
import time

mail_ru_domains = [
    'mail.ru',
    'inbox.ru',
    'list.ru',
    'bk.ru'
]

proxy = dict()
last_time_proxy = 0
fromAddress = 'checker@checker.io'
mail_ru_api = 'https://account.mail.ru/api/v1/user/exists?email={}'
regex = '[^@]+@[^@]+\.[^@]+'
def check_syntax(email):
    # Syntax check
    match = re.match(regex, email)
    if match == None:
        return False
    return True

def get_domain(email):
    # Getting a domain
    splitAddress = email.split('@')
    domain = str(splitAddress[1])
    return domain

def check_address(email, domain):
    # MX record lookup
    try: # Return false if no such domain or wrong MX record
        records = dns.resolver.query(domain, 'MX')
    except:
        return False
    mxRecord = records[0].exchange
    mxRecord = str(mxRecord)

    # SMTP lib setup (use debug level for full output)
    server = smtplib.SMTP()
    server.set_debuglevel(0)

    # SMTP Conversation
    server.connect(mxRecord)
    server.helo(server.local_hostname) ### server.local_hostname(Get local server hostname)
    server.mail(fromAddress)
    code, message = server.rcpt(str(email))
    server.quit()

    # Assume SMTP response 250 is success
    if code == 250:
        return True
    else:
        return False

def check_mail_ru(email):
    # Function for checking mail.ru accounts
    while True: # Make sure that we will get the validation status
        try:
            req = requests.get(mail_ru_api.format(email), proxies=proxy)
            data = req.json()
        except:
            get_proxy()
            continue

        if data['status'] == 403: # Brute-forcing a valid proxy
            get_proxy()
            continue
        if data['body']['exists']:
            return True
        else:
            return False

def write_to_file(msg, valid, log):
    # Module for some sort of logging
    base_name = '{}_emails{}.txt'
    if valid:
        base_name = base_name.format('valid', '')
    else:
        base_name = base_name.format('invalid', '')
    
    if log:
        base_name = base_name.format('_log')
    
    with open(base_name, 'a') as f:
        f.write(msg + '\n')
        print(msg + '\n')

def get_proxy():
    print('Retrieving new proxy...')
    # Function for getting proxy from getproxylist.com
    global proxy
    get_proxy = requests.get('https://api.getproxylist.com/proxy?protocol=http&apiKey=632720e8fcde28dda37860c2ccff2d619dd36f86').json() # only http
    proxy = {
        'http': '{}://{}:{}'.format(get_proxy['protocol'], get_proxy['ip'], get_proxy['port']),
        'https': '{}://{}:{}'.format(get_proxy['protocol'], get_proxy['ip'], get_proxy['port'])
    } # Form a valid dict for requsts library
    with open('used_proxies.txt', 'a') as f: # Logging used proxies
        f.write(str(proxy) + '\n')

def main(email):
    # Run checking for a specific email
    email = email.strip()
    print('Checking: {}'.format(email)) # Some sort of logging. TODO: Better logging system

    syntax = check_syntax(email) # Check email syntax
    if not syntax:
        write_to_file(
            '{} :: {}'.format(email, 'Syntax is invalid'),
            False,
            True
        )
        return

    domain = get_domain(email) # Get email's domain
    if domain in mail_ru_domains: # Check if the email is registered on mail.ru
        is_email_exists = check_mail_ru(email) # Run mail.ru checking
        if is_email_exists:
            write_to_file(
                '{}'.format(email),
                True,
                False
            )
        else:
            write_to_file(
                '{} :: {}'.format(email, 'Inbox does not exist'),
                False,
                True
            )
        return

    is_email_exists = check_address(email, domain) # Run classic checking(based on SMTP)
    if is_email_exists:
        write_to_file(
            '{}'.format(email),
            True,
            False
        )
    else:
        write_to_file(
            '{} :: {}'.format(email, 'Inbox does not exist'),
            False,
            True
        )

# Parsing threads amount from configuration file
config = configparser.ConfigParser()
config.read('config.ini')
setting_threads = config['GENERAL']['threads']
setting_threads = int(setting_threads) + 1

with open('email.txt', 'r') as f: # Read email from file
    emails = f.readlines()

def spawn_threads(emails):
    # Generator of trades, allows to achive the best performance
    for email in emails:
        thread = threading.Thread(target=main, args=(email,))
        yield thread

get_proxy() # init proxy settings before running anything
threads = spawn_threads(emails)
for thread in threads:
    while(threading.active_count() >= setting_threads): # Prevent from running more that necessary
        pass 
    thread.start()
