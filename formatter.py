with open('invalid_emails.txt', 'r') as f:
    emails = f.readlines()

formatted_emails = list()
for email in emails:
    email, message = email.split('::')
    formatted_emails.append(email.strip())

with open('formatted_invalids.txt', 'a') as f:
    for email in formatted_emails:
        f.write(email + '\n')

print('done')